FROM node:alpine 

WORKDIR /app

COPY package-lock.json .
COPY package.json .

RUN npm install

COPY . .
EXPOSE 4200
CMD /app/node_modules/.bin/ng serve --host 0.0.0.0 --disable-host-check